<?php get_header(); ?>
<div class="context">

		<?php if (have_posts()) : ?>

 	  <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
     <?php if (is_category()) { ?>
    <h2 class="pagetitle"><?php single_cat_title(); ?></h2>
     <?php } elseif( is_tag() ) { ?>
    <h2 class="pagetitle">Články se štítkem &#8218;<?php single_tag_title(); ?>&#8217;</h2>
     <?php } elseif (is_day()) { ?>
    <h2 class="pagetitle">Archiv: <?php the_time('j. n. Y (l)'); ?></h2>
     <?php } elseif (is_month()) { ?>
    <h2 class="pagetitle">Archiv: <?php the_time('F Y'); ?></h2>
     <?php } elseif (is_year()) { ?>
    <h2 class="pagetitle">Archiv: <?php the_time('Y'); ?></h2>
    <?php } elseif (is_author()) { ?>
    <h2 class="pagetitle">Archiv dle autora</h2>
     <?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
    <h2 class="pagetitle">Archiv dle blogu</h2>
     <?php } ?>
		

	<?php while (have_posts()) : the_post(); ?> 
<aside>
<time datetime="<?php the_time('Y-m-d') ?>" pubdate><span><?php the_time('d.m') ?></span><?php the_time('Y') ?></time>
<p>
<?php
$titulek = the_title('','',false);
$obsah = get_the_content();

$titulek = preg_replace('#https?://([a-zA-Z0-9-_./\?=&]+)#i', '<a href="$0">$1</a>', $titulek);
$titulek = preg_replace('@(^|[^&])(#([a-zA-Z0-9-_]+))@i', '$1<a href="'.get_option('tag_base').'$3">$2</a>', $titulek);
$titulek = preg_replace('#@([a-zA-Z0-9-_]+)#i', '<a href="http://www.twitter.com/$1">$0</a>', $titulek);
if(strlen($obsah)>1){
?>
  <a href="<?php the_permalink() ?>" rel="bookmark" title="Trvalý odkaz: <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
<?php

} else {
  echo $titulek;
}
?>
</aside>  

    <?php endwhile; ?>
    <div class="moreinfo">
<?php next_posts_link(__('Older Entries »')); ?>
<?php previous_posts_link(__('Newer Entries »')); ?>
</div>
    <?php endif; ?>

</div>

<?php get_footer(); ?>
