<?php get_header(); ?>
<div id="content">
<?php if (have_posts()) : ?>

<h2>Výsledek hledání</h2>

<?php while (have_posts()) : the_post(); ?>
	
<article>
<h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Trvalý odkaz: <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
<time datetime="<?php the_time('Y-m-d') ?>" pubdate><span><?php the_time('d.m.') ?></span><?php the_time('Y') ?></time>
<p>
<?php the_content('<b>Pokračovat ve čtení &raquo;</b>'); ?>
<?php
}
?>
<?php edit_post_link('Upravit', ' ', ''); ?>  
</p>
</article>

<?php endwhile; ?>
<div class="moreinfo">
<?php next_posts_link(__('Older Entries »')); ?>
<?php previous_posts_link(__('Newer Entries »')); ?>
</div>
<?php endif; ?>

</div>

<?php get_footer(); ?>
