	<div class="rightbar">
	    <div class="other">
		<?php get_sidebar ('other'); ?>
	    </div>
            <div class="issues">
                <?php get_sidebar ('issues'); ?>
            </div>
            <div class="partners">
                <?php get_sidebar ('partner'); ?>
            </div>
            <div class="tools">
		<?php get_sidebar ('tools'); ?>
            </div>
        </div>
</div> <!-- .container --> 

    <footer>
<?php wp_footer(); ?>
      <?php get_sidebar('footer'); ?>
&copy;<?php the_time('Y'); ?>&nbsp;<a href="<?php echo get_settings('home'); ?>"><?php bloginfo('name'); ?></a>, 
<a href="<?php bloginfo('rss2_url'); ?>">RSS</a>, <a href="<?php bloginfo('comments_rss2_url'); ?>">RSS komentářů</a>.
    </footer>
  </div>
  </body>
</html>

