<!DOCTYPE html>
<html lang="<?php language_attributes(); ?>">
  <head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="robots" content="index,follow">
    <?php if(!is_home()):?>
    <title><?php wp_title('&laquo;', true, 'right'); ?> - <?php bloginfo('name'); ?></title>
    <?php else : ?>
    <title><?php bloginfo('name'); ?></title>
    <?php endif; ?>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />
   <!-- <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/bootstrap/css/bootstrap.min.css" type="text/css" media="screen" />  /-->
   <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css"/>
   <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/font/css/font-awesome.min.css">
   <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
   <link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
   <link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
   <link href="https://plus.google.com/100078467202260567543/" rel="publisher" />
   <link href="https://plus.google.com/+OpenAlt" rel="publisher" />
    <link rel="icon" href="/spolek/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="/spolek/favicon.png" type="image/png" />
   <?php wp_head(); ?>
  </head>
  <body>
    <div id="page">
    	<?php wp_nav_menu( array( 'theme_location' => 'header-menu','container'=>'nav','container_class' => 'menu','items_wrap' => '<div class="container"><ul class="nav">%3$s</ul>
<ul class="nav navbar-nav navbar-right">
  <li> <a href="https://www.facebook.com/OpenAlt"> <i class="fa fa-facebook"></i> </a> </li>
  <li> <a href="https://twitter.com/OpenAlt"> <i class="fa fa-twitter"></i> </a> </li>
  <li> <a href="https://plus.google.com/+OpenAlt/posts"> <i class="fa fa-google-plus"></i> </a> </li>
  <li> <a href="https://www.linkedin.com/groups?home=&gid=3707492&trk=anet_ug_hm"> <i class="fa fa-linkedin"></i> </a> </li>
</ul>
</div>
') ); ?>


    <div id="header">
      <div class="logo"><a href="<?php echo get_option('home'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/logo.svg" alt="<?php bloginfo('name'); ?>"/></a></div>
      <div class="ad"><?php get_sidebar ('attention'); ?></div>
   </div>
<div class="container">
