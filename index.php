<?php get_header(); ?>
<div class="context">
    <div id="centerbar">
      <div class="zapisky">
      <!--- NEWS --->
<?php 
  $news = new WP_Query( array( 'post_type' => array('aside','asides')) );
  if ($news->have_posts()) :
    while ($news->have_posts()) : $news->the_post(); ?>
<aside>
<time datetime="<?php the_time('Y-m-d') ?>" pubdate><span><?php the_time('d.m') ?></span><?php the_time('Y') ?></time>
<p>
<?php
$titulek = the_title('','',false);
$obsah = get_the_content();

$titulek = preg_replace('#https?://([a-zA-Z0-9-_./\?=&]+)#i', '<a href="$0">$1</a>', $titulek);
$titulek = preg_replace('@(^|[^&])(#([a-zA-Z0-9-_]+))@i', '$1<a href="'.get_option('tag_base').'$3">$2</a>', $titulek);
$titulek = preg_replace('#@([a-zA-Z0-9-_]+)#i', '<a href="http://www.twitter.com/$1">$0</a>', $titulek);
if(strlen($obsah)>1){
?>
  <a href="<?php the_permalink() ?>" rel="bookmark" title="Trvalý odkaz: <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
<?php

} else {
  echo $titulek;
}
?>
</p>
</aside>

<?php endwhile; ?>
<div class="moreinfo">
<?php
echo '<a href="';
	echo get_bloginfo('url').'?post_type=asides';
  echo '">'.__('Více').' &raquo;</a>';
?>
</div>
<?php endif; ?>
      </div>
      <?php get_sidebar ('centerbar'); ?>
    </div>
<div id="content">
  <div class="articles">
    <?php if ($wp_query->have_posts()) : ?>
    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

    <article>
      <h1><a href="<?php the_permalink() ?>" rel="bookmark" title="Trvalý odkaz: <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
      <time datetime="<?php the_time('Y-m-d') ?>" pubdate>(<?php the_time('d.m.Y') ?>)</time>
      <span>
      <?php 
	the_content();
	edit_post_link('Upravit', ' ', '');
      ?>  
      </span>
    </article>
    <?php endwhile; ?>
    <div class="moreinfo">
      <?php next_posts_link(__('Older Entries »')); ?>
      <?php previous_posts_link(__('Newer Entries »')); ?>
    </div>
  </div>
<?php endif;?>
</div>
</div>

<?php get_footer(); ?>