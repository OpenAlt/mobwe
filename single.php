<?php get_header(); ?>
<div class="context">
<div id="content">
<article>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div class="direction">
<?php previous_post_link('&laquo; %link') ?>&nbsp;
<?php next_post_link('%link &raquo;') ?>
</div>
  <h1><?php the_title(); ?></h1>
<p><?php the_author_posts_link(); ?> – <time datetime="<?php the_time('Y-m-d') ?>" pubdate><?php the_time('d.m.Y') ?></time> (<?php the_tags('',' ',''); ?> <?php the_category(' '); ?>)
<ul class='translations'>
 <?php pll_the_languages(array('post_id' => $post->ID, 'show_flags' => 1, 'hide_if_no_translation' => 1, 'hide_current' => 1)); ?>
</ul>
</p>
    <?php the_content('<p>Pokračuj ve čtení &raquo;</p>'); ?>
<?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
      ?>
      Můžete 
      <a href="#koment">zanechat komentář</a>.
<?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
      ?> Komentáře jsou nyní uzavřeny.
<?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
      ?>
      Můžete zanechat svůj komentář.
      <?php } edit_post_link('Editovat','','.'); ?></small>

    <?php comments_template(); ?>
    <?php endwhile; else: ?>
    <p>
      Článek nenalezen.
    </p>
    <?php endif; ?>
</article>
</div>
</div>
  <?php get_footer(); ?>
