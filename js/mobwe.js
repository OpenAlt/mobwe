var page = 1;
var tpage;
var page_size;
var article = new Array();

function ChangeFormat(what){
    $(function(){
      
      var header = $('#logo').height();
      var footer = $('footer').height();
      var hwin = $(window).height();
      var whsize = hwin - (header+footer);
      var wwsize = $('#' + what).width();
      var column_size = wwsize/2 - 10
      /*$('#' + what).css({'height': whsize,'max-height':whsize});*/
      $('#' + what + ' article').css({'column-count': '2',
			  '-webkit-column-count': '2',
			  '-moz-column-count': '2'
      });
      var kde = $('#aend').position().left;
      var num_pages = kde / wwsize;
      page_size = wwsize;
      tpage = num_pages;
      $('#content_post article *').each( function (index,elem){ //do pole si ulozime pozici prvku v DOMu
	article.push({e: elem,l: $(elem).position().left,d:$(elem).css('display')});
	//$(elem).css('display','none');
      });
      if(num_pages > 1){
	$('#' + what).append('<a href="#" onClick="move(-1)">&lt;&lt;</a>&nbsp;<a href="#" onClick="move(1)">&gt;&gt;</a>');
      }
    });
}

function move(direction){
  var start, stop;
  console.log(page + '/' + tpage);
  
  if(direction > 0){
    start = page*page_size;
    stop = start + page_size;
    if(page > tpage) return 1;
    page++;
    
  } else {
    stop = page*page_size;
    start = stop - page_size;
    if(page <= 0) return 1;
    page--;
  }
  console.log('Start ' + start + ' ' + page + '-' + page_size);
  
  for(var i=0;i<article.length;i++){
    var elem = article[i];
    if(elem.e.id == "aend") continue;
    if( elem.l <= start ){
      $(elem.e).css('display','none'); //schovame vse nalevo
    } else {
      $(elem.e).css('display',elem.d); //ostatni muze byt viditelne
    }
  };
}

function loadJabber(){
  var d = new Date();
  $.ajax({
    url: "http://conf.openmobility.cz/conf@openmobility.cz/" + d.getFullYear() + "/" + (d.getMonth()+1) + "/" + (d.getDate()) + ".html",
    cache: false,
}).done(function(data){
  var kam = $('#chat code');
  var co = $($(data).find('span')[0]).parent();
  kam.html(co.html());
});
  
}

//$('document').ready(loadJabber());
