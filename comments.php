<?php if ( have_comments() ) : ?>
	

	<ol>
	<?php wp_list_comments(); ?>
	</ol>

	
<?php previous_comments_link() ?>
<?php next_comments_link() ?>

 

	<?php if ( comments_open() ) : ?>
		

	 <?php else : // comments are closed ?>
		<p>
    Komentáře jsou uzavřeny.
    </p>

	<?php endif; ?>
<?php endif; ?>



<?php if ( comments_open() ) : ?>


<a name="koment"></a>
<h3><?php comment_form_title( 'Přidej komentář', 'Přidej komentář k %s' ); ?></h3>


<!-- tato část se zobrazí v případě, že jsou komentáře povoleny jen registrovaným 
uživatelům -->
<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
<p>Musíte se <a href="<?php echo wp_login_url( get_permalink() ); ?>">přihlásit</a>, abyste mohli komentovat.</p>
<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">


<?php if ( is_user_logged_in() ) : ?>


<p>Jsi přihlášen jako<a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Odhlásit">Odhlásit &raquo;</a></p>

<?php else : ?>

<p><input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
<label for="author"><small>Jméno <?php if ($req) echo "(musíte zadat)"; ?></small></label></p>

<p><input type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
<label for="email"><small>E-mail (nebude zveřejněn) <?php if ($req) echo "(musíte zadat)"; ?></small></label></p>

<p><input type="text" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" size="22" tabindex="3" />
<label for="url"><small>Web</small></label></p>

<?php endif; ?>

<p><small><strong>XHTML:</strong> Můžete použít tyto značky: 
<code><?php echo allowed_tags(); ?></code></small></p>

<p><textarea name="comment" id="comment" cols="90%" rows="10" tabindex="4"></textarea></p>

<p><input name="submit" type="submit" id="submit" tabindex="5" value="Odeslat komentář" />
<?php comment_id_fields(); ?>
</p>
<?php do_action('comment_form', $post->ID); ?>

</form>

<?php endif; // If registration required and not logged in ?>
</div>

<?php endif; // if you delete this the sky will fall on your head ?>
