<?php

add_action( 'widgets_init', 'my_register_sidebars' );

function my_register_sidebars() {

	/* Register the 'primary' sidebar. */
	register_sidebar(
		array(
			'id' => 'tools',
			'name' => __( 'Other' ),
			'description' => __( 'Prostor pro servisní menu.' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
		register_sidebar(
		array(
			'id' => 'attention',
			'name' => __( 'attention' ),
			'description' => __( 'Prostor vpravo vedle loga.' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
	register_sidebar(
		array(
			'id' => 'centerbar',
			'name' => __( 'Centerbar' ),
			'description' => __( 'Prostor pod zprávičkami nad obsahem.' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
	register_sidebar(
		array(
			'id' => 'partner',
			'name' => __( 'Partner' ),
			'description' => __( 'Prostor pro partnery.' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
		register_sidebar(
		array(
			'id' => 'issues',
			'name' => __( 'Řešíme' ),
			'description' => __( 'Chlívek pro umisťování informací co řešíme.' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);
	register_sidebar(
		array(
			'id' => 'other',
			'name' => __( 'Tools' ),
			'description' => __( 'Prostor v pravo nad partnery a co řešíme.' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget' => '</section>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>'
		)
	);

	/* Repeat register_sidebar() code for additional sidebars. */
}

add_action( 'after_setup_theme', 'mobwe_setup' );

if ( ! function_exists( 'mobwe_setup' ) ){
  function mobwe_setup(){
    add_theme_support( 'post-formats', array( 'aside', 'page','post') );
  }
}

function my_search_form( $form ) {

    $form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <div><label class="screen-reader-text" for="s">' . __('Search for:') . '</label>
    <input type="search" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
    </div>
    </form>';

    return $form;
}

function register_my_menu() {
  register_nav_menu('header-menu',__( 'Hlavní nabídka' ));
}
add_action( 'init', 'register_my_menu' );
add_filter( 'get_search_form', 'my_search_form' );
?>
